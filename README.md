La criptografía asimétrica (en inglés asymmetric key cryptography),
también llamada criptografía de clave pública (en inglés public key cryptography)
o criptografía de dos claves1​(en inglés two-key cryptography), es el método
criptográfico que usa un par de claves para el envío de mensajes.
Las dos claves pertenecen a la misma persona que ha enviado el mensaje.
Una clave es pública y se puede entregar a cualquier persona, la otra clave
es privada y el propietario debe guardarla de modo que nadie tenga acceso a
ella. Además, los métodos criptográficos garantizan que esa pareja de claves 
sólo se puede generar una vez, de modo que se puede asumir que no es posible 
que dos personas hayan obtenido casualmente la misma pareja de claves.

La criptografía simétrica (en inglés symmetric key cryptography), también 
llamada criptografía de clave secreta (en inglés secret key cryptography)
o criptografía de una clave1​ (en inglés single-key cryptography),
es un método criptográfico en el cual se usa una misma clave para cifrar 
y descifrar mensajes en el emisor y el receptor. Las dos partes que se 
comunican han de ponerse de acuerdo de antemano sobre la clave a usar. 
Una vez que ambas partes tienen acceso a esta clave, el remitente cifra un 
mensaje usando la clave, lo envía al destinatario, y éste lo descifra con 
la misma clave.

En criptografía, las expresiones autoridad de certificación, o certificadora,
o certificante, o las siglas AC o CA (por la denominación en idioma inglés
Certification Authority), señalan a una entidad de confianza, responsable
de emitir y revocar los certificados, utilizando en ellos la firma electrónica,
para lo cual se emplea la criptografía de clave pública. 

User Datagram Protocol (UDP) es un protocolo del nivel de transporte basado en 
el intercambio de datagramas (Encapsulado de capa 4 o de Transporte del Modelo OSI)
. Permite el envío de datagramas a través de la red sin que se haya establecido
previamente una conexión, ya que el propio datagrama incorpora suficiente información
de direccionamiento en su cabecera. Tampoco tiene confirmación ni control de flujo, 
por lo que los paquetes pueden adelantarse unos a otros; y tampoco se sabe si ha
llegado correctamente, ya que no hay confirmación de entrega o recepción. 

The Internet protocol suite is the conceptual model and set of communications protocols
used on the Internet and similar computer networks. It is commonly known as TCP/IP because
the foundational protocols in the suite are the Transmission Control Protocol (TCP) and the 
Internet Protocol (IP).The Internet protocol suite provides end-to-end data communication 
specifying how data should be packetized, addressed, transmitted, routed, and received.

El modelo de interconexión de sistemas abiertos (ISO/IEC 7498-1), más conocido como “modelo OSI”
, (en inglés, Open System Interconnection) es un modelo de referencia para los protocolos de la 
red de arquitectura en capas. Consiste de 7 capas:
Aplicación->Servicios de red a aplicaciones
Presentación -> Representación de los datos
Sesión ->Comunicación entre dispositivos
Transporte->Conexion entre los extremos 
Red ->Ruta y direcciamineto logico
Enlace de datos ->Direccionamiento fisico
Física->Señal y transmision binaria.

En criptografía, RSA (Rivest, Shamir y Adleman) es un sistema criptográfico de clave pública 
desarrollado en 1977. Es el primer y más utilizado algoritmo de este tipo y es válido tanto 
para cifrar como para firmar digitalmente.
La seguridad de este algoritmo radica en el problema de la factorización de números enteros. 
Los mensajes enviados se representan mediante números, y el funcionamiento se basa en el producto,
conocido, de dos números primos grandes elegidos al azar y mantenidos en secreto. Actualmente 
estos primos son del orden de 
10 200, y se prevé que su tamaño crezca con el aumento de la capacidad de cálculo de los ordenadores.
Como en todo sistema de clave pública, cada usuario posee dos claves de cifrado: una pública y otra privada.
Cuando se quiere enviar un mensaje, el emisor busca la clave pública del receptor, cifra su mensaje con esa 
clave, y una vez que el mensaje cifrado llega al receptor, este se ocupa de descifrarlo usando su clave privada.

DSA (Digital Signature Algorithm, en español Algoritmo de Firma digital) es un estándar del Gobierno Federal de 
los Estados Unidos de América o FIPS para firmas digitales. Fue un Algoritmo propuesto por el Instituto Nacional 
de Normas y Tecnología de los Estados Unidos para su uso en su Estándar de Firma Digital (DSS), especificado en 
el FIPS 186. DSA se hizo público el 30 de agosto de 1991, este algoritmo como su nombre lo indica, sirve para
firmar y no para cifrar información. Una desventaja de este algoritmo es que requiere mucho más tiempo de cómputo
que RSA.